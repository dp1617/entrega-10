
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Chorbi;
import domain.CreditCard;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ChorbiServiceTest extends AbstractTest {

	// Services and repositories

	//@Autowired
	//private ActorService	actorService;
	@Autowired
	private ChorbiService	chorbiService;


	// Templates --------------------------------------------------------------

	/*
	 * "An actor who is authenticated as a chorbi must be able to:
	 * - Edit his or her profile."
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de editar un perfil.
	 * Para forzar el error pueden darse varios casos, como son:
	 * 
	 * � La persona autentificada es un administrador
	 * � La persona no est� autentificada
	 * � Alguno de los atributos no tiene el formato correcto o no es v�lido
	 * � La Id del chorbi no existe
	 */
	public void profileEdit(final String username, final String city, final String country, final String genre, final String kindOfRelationship, final String brandName, final Integer expirationMonth, final Integer expirationYear, final Integer cvvCode,
		final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos si la persona autentificada es chorbi
			this.chorbiService.checkIfChorbi();

			// Inicializamos los atributos para la edici�n
			Chorbi chorbi;
			CreditCard creditCard;

			chorbi = this.chorbiService.findByPrincipal();
			creditCard = chorbi.getCreditCard();

			chorbi.setCity(city);
			chorbi.setCountry(country);
			chorbi.setGenre(genre);
			chorbi.setKindOfRelationship(kindOfRelationship);

			creditCard.setBrandName(brandName);
			creditCard.setExpirationMonth(expirationMonth);
			creditCard.setExpirationYear(expirationYear);
			creditCard.setCvvCode(cvvCode);

			chorbi.setCreditCard(creditCard);

			// Comprobamos atributos

			// Guardamos
			this.chorbiService.save(chorbi);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------
	@Test
	public void driverProfileEdit() {

		final Object testingData[][] = {
			// Editar profile con administrador autentificado -> false
			{
				"admin", "city", "country", "man", "love", "VISA", 7, 2018, 854, IllegalArgumentException.class
			},
			// Editar profile sin autentificar -> false
			{
				null, "city", "country", "man", "love", "VISA", 7, 2018, 854, IllegalArgumentException.class
			},
			//			// Editar profile con kindOfRelationship no existente -> false
			//			{
			//				"chorbi1", "city", "country", "man", "notValid", "VISA", 7, 2018, 854, IllegalArgumentException.class
			//			},
			//			// Editar profile con genre no existente -> false
			//			{
			//				"chorbi1", "city", "country", "none", "love", "VISA", 7, 2018, 854, IllegalArgumentException.class
			//			},
			// Editar profile con brandName no v�lido -> false
			{
				"chorbi1", "city", "country", "man", "love", "VISAAAAEAA", 7, 2018, 854, IllegalArgumentException.class
			},
			// Editar profile con expirationYear no v�lido -> false
			{
				"chorbi1", "city", "country", "man", "love", "VISA", 7, 1992, 854, IllegalArgumentException.class
			},
			// Editar profile con cvv no v�lido -> false
			{
				"chorbi1", "city", "country", "man", "love", "VISA", 7, 2018, 4, IllegalArgumentException.class
			},
			// Editar profile de chorbi(1) y atributos correctos (todos) -> true
			{
				"chorbi1", "city", "country", "man", "love", "VISA", 7, 2018, 854, null
			},
			// Editar profile de chorbi(2) y atributos correctos (todos) -> true
			{
				"chorbi2", "city", "country", "woman", "love", "VISA", 7, 2018, 854, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.profileEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Integer) testingData[i][6], (Integer) testingData[i][7],
				(Integer) testingData[i][8], (Class<?>) testingData[i][9]);
	}
}
