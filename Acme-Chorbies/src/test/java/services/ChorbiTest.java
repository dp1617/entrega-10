
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ChorbiTest extends AbstractTest {

	@Autowired
	private ChorbiService	chorbiService;


	/*
	 * Vista para ver todos los Chorbies
	 * 
	 * En este caso de uso se listan los chorbies desde su correspondiente vista
	 * Para provocar el error intentamos acceder
	 * al listado de chorbies sin autenticarnos.
	 */
	public void listChorbiTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.chorbiService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverListChorbiTest() {
		final Object testingData[][] = {
			// Se accede con admin -> true
			{
				"admin", null
			},
			// Se accede con chorbi -> true
			{
				"chorbi1", null
			},
			//  Se accede con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listChorbiTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

}
