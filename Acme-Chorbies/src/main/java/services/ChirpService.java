
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.ChirpRepository;
import domain.Chirp;
import domain.Chorbi;

@Service
@Transactional
public class ChirpService {

	// Managed repository
	@Autowired
	private ChirpRepository	chirpRepository;


	// Constructors
	public ChirpService() {
		super();
	}


	// Supporting services
	@Autowired
	private ChorbiService	chorbiService;


	// Simple CRUD methods
	public Chirp create() {
		this.chorbiService.checkIfChorbi();

		Chirp chirp;
		chirp = new Chirp();

		Chorbi chorbi;
		Date now;

		chorbi = this.chorbiService.findByPrincipal();
		now = new Date(System.currentTimeMillis() - 1000);

		chirp.setSentMoment(now);
		chirp.setSender(chorbi);

		chirp.setFromChorbi("fromChorbi");
		chirp.setToChorbi("toChorbi");

		return chirp;
	}

	public Collection<Chirp> findAll() {
		Collection<Chirp> chirps;
		chirps = this.chirpRepository.findAll();
		Assert.notNull(chirps);
		return chirps;
	}

	public Chirp findOne(final int chirpId) {
		Assert.isTrue(chirpId != 0);
		Chirp result;
		result = this.chirpRepository.findOne(chirpId);
		Assert.notNull(result);
		return result;
	}

	public Chirp save(final Chirp chirp) {
		this.chorbiService.checkIfChorbi();
		Chirp result;
		Assert.notNull(chirp);
		this.checkChirp(chirp);
		this.checkAttachments(chirp.getAttachments());
		result = this.chirpRepository.save(chirp);
		return result;
	}

	public void delete(final Chirp chirp) {
		this.chorbiService.checkIfChorbi();
		Assert.notNull(chirp);
		this.chirpRepository.delete(chirp);
	}

	// Other bussines methods

	public void checkChirp(final Chirp m) {
		Boolean result = true;
		if (m.getRecipient() == null || m.getSender() == null || m.getText() == null || m.getSubject() == null)
			result = false;
		Assert.isTrue(result);
	}

	// En el caso de reenviar no comprueba sender ni recipient porque los asignara de nuevo
	public void checkChirpForward(final Chirp m) {
		Boolean result = true;
		if (m.getText() == null || m.getSubject() == null)
			result = false;
		Assert.isTrue(result);
	}

	public void checkAttachments(final Collection<String> attachments) {
		final String regexp = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		for (final String s : attachments)
			Assert.isTrue(s.matches(regexp), "El formato de los adjuntos no es correcto");
	}

	public void checkReply(final Chirp m) {
		Boolean result = true;
		final Chorbi chorbi = this.chorbiService.findByPrincipal();
		if (!chorbi.getReceivedChirps().contains(m))
			result = false;
		Assert.isTrue(result);
	}

	public void checkForward(final Chirp m) {
		Boolean result = true;
		final Chorbi chorbi = this.chorbiService.findByPrincipal();
		if (!(chorbi.getReceivedChirps().contains(m) || chorbi.getSentChirps().contains(m)))
			result = false;
		Assert.isTrue(result);
	}

	public Chirp reconstruct(final Chirp m) {
		final Chorbi sender = this.chorbiService.findByPrincipal();
		final Chirp chirp1 = new Chirp();
		final Chirp chirp2 = new Chirp();
		final String fromChorbi = sender.getName() + " " + sender.getSurname();
		final String toChorbi = m.getRecipient().getName() + " " + m.getRecipient().getSurname();

		chirp1.setId(m.getId());
		chirp1.setVersion(m.getVersion());

		chirp1.setFromChorbi(fromChorbi);
		chirp1.setToChorbi(toChorbi);

		chirp1.setSubject(m.getSubject());
		chirp1.setText(m.getText());
		chirp1.setSentMoment(m.getSentMoment());
		chirp1.setAttachments(m.getAttachments());
		chirp1.setSender(sender);
		chirp1.setRecipient(m.getRecipient());

		// Esto es un simple separador para mejor visualización

		chirp2.setId(chirp1.getId() + 1);
		chirp2.setVersion(chirp1.getVersion() + 1);

		chirp2.setFromChorbi(chirp1.getFromChorbi());
		chirp2.setToChorbi(chirp1.getToChorbi());

		chirp2.setSubject(chirp1.getSubject());
		chirp2.setText(chirp1.getText());
		chirp2.setSentMoment(chirp1.getSentMoment());
		chirp2.setAttachments(chirp1.getAttachments());
		chirp2.setSender(chirp1.getSender());
		chirp2.setRecipient(chirp1.getRecipient());

		m.getSender().getSentChirps().add(chirp1);
		m.getRecipient().getReceivedChirps().add(chirp2);

		this.chorbiService.save(sender);
		this.chorbiService.save(m.getRecipient());

		if (sender.getId() == m.getRecipient().getId())
			throw new IllegalArgumentException("You cannot send a chirp to yourself");
		return chirp1;
	}

	public void deleteReceived(final Chirp chirp) {
		this.chorbiService.checkIfChorbi();
		Assert.notNull(chirp);
		final Chorbi recipient = this.chorbiService.findByPrincipal();
		Assert.isTrue(recipient.getId() == chirp.getRecipient().getId());

		//Actualizar a null el campo recipient_id en BD para ocultar el mensaje de la vista

		if (chirp.getSender() == null)
			this.chirpRepository.delete(chirp);
		else
			chirp.setRecipient(null);
	}

	public void deleteSent(final Chirp chirp) {
		this.chorbiService.checkIfChorbi();
		Assert.notNull(chirp);
		final Chorbi sender = this.chorbiService.findByPrincipal();
		Assert.isTrue(sender.getId() == chirp.getSender().getId());

		if (chirp.getRecipient() == null)
			this.chirpRepository.delete(chirp);
		else
			chirp.setSender(null);
	}

	public Chirp reply(final int chirpId) {
		Chirp result;
		final Chirp m = this.findOne(chirpId);

		//this.checkChirp(m);
		this.checkReply(m);

		result = this.create();

		result.setRecipient(m.getSender());
		return result;
	}

	public Chirp forward(final int chirpId) {
		Chirp result;
		final Chirp m = this.findOne(chirpId);

		this.checkChirpForward(m);
		this.checkForward(m);

		result = this.create();

		result.setSubject("FW: " + m.getSubject());
		result.setText(m.getText());
		result.setAttachments(m.getAttachments());

		return result;
	}

	// Get all existing chorbies
	public Collection<Chorbi> getRecipients() {
		Chorbi chorbi;
		Collection<Chorbi> chorbies = new ArrayList<>();

		chorbi = this.chorbiService.findByPrincipal();
		chorbies = this.chorbiService.findAll();

		chorbies.remove(chorbi);

		return chorbies;
	}
}
