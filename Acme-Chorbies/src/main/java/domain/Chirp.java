
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Chirp extends DomainEntity {

	// Own attributes

	private String				fromChorbi;
	private String				toChorbi;
	private Date				sentMoment;
	private String				subject;
	private String				text;
	private Collection<String>	attachments;


	// Constructor

	public Chirp() {
		super();
	}

	// Getters and setters 

	@NotBlank
	public String getFromChorbi() {
		return this.fromChorbi;
	}
	public void setFromChorbi(final String fromChorbi) {
		this.fromChorbi = fromChorbi;
	}

	@NotBlank
	public String getToChorbi() {
		return this.toChorbi;
	}
	public void setToChorbi(final String toChorbi) {
		this.toChorbi = toChorbi;
	}

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getSentMoment() {
		return this.sentMoment;
	}
	public void setSentMoment(final Date sentMoment) {
		this.sentMoment = sentMoment;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getSubject() {
		return this.subject;
	}
	public void setSubject(final String subject) {
		this.subject = subject;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getText() {
		return this.text;
	}
	public void setText(final String text) {
		this.text = text;
	}

	@ElementCollection
	public Collection<String> getAttachments() {
		return this.attachments;
	}
	public void setAttachments(final Collection<String> attachments) {
		this.attachments = attachments;
	}


	// Relationships

	private Chorbi	sender;
	private Chorbi	recipient;


	@Valid
	@ManyToOne(optional = true)
	public Chorbi getSender() {
		return this.sender;
	}
	public void setSender(final Chorbi sender) {
		this.sender = sender;
	}

	@Valid
	@ManyToOne(optional = true)
	public Chorbi getRecipient() {
		return this.recipient;
	}
	public void setRecipient(final Chorbi recipient) {
		this.recipient = recipient;
	}

}
