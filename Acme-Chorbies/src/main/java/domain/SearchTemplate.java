
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class SearchTemplate extends DomainEntity {

	// Own attributes

	private String	kindOfRelationship;
	private Integer	estimatedAge;
	private String	genre;
	private String	keyword;
	private String	country;
	private String	state;
	private String	province;
	private String	city;
	private Date	lastTimeUsed;


	// Constructor

	public SearchTemplate() {
		super();
	}

	// Gets and sets

	@Pattern(regexp = "^$|^(activities|friendship|love)$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getKindOfRelationship() {
		return this.kindOfRelationship;
	}
	public void setKindOfRelationship(final String kindOfRelationship) {
		this.kindOfRelationship = kindOfRelationship;
	}

	@Min(18)
	public Integer getEstimatedAge() {
		return this.estimatedAge;
	}
	public void setEstimatedAge(final Integer estimatedAge) {
		this.estimatedAge = estimatedAge;
	}

	@Pattern(regexp = "^$|^(man|woman)$")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getGenre() {
		return this.genre;
	}
	public void setGenre(final String genre) {
		this.genre = genre;
	}

	@Pattern(regexp = "^$|\\w+")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getKeyword() {
		return this.keyword;
	}
	public void setKeyword(final String keyword) {
		this.keyword = keyword;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getCountry() {
		return this.country;
	}
	public void setCountry(final String country) {
		this.country = country;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getState() {
		return this.state;
	}
	public void setState(final String state) {
		this.state = state;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getProvince() {
		return this.province;
	}
	public void setProvince(final String province) {
		this.province = province;
	}

	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getCity() {
		return this.city;
	}
	public void setCity(final String city) {
		this.city = city;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	public Date getLastTimeUsed() {
		return this.lastTimeUsed;
	}
	public void setLastTimeUsed(final Date lastTimeUsed) {
		this.lastTimeUsed = lastTimeUsed;
	}


	// Relationships

	private Collection<Chorbi>	foundChorbies;
	private Chorbi				chorbi;


	@Valid
	@ManyToMany
	public Collection<Chorbi> getFoundChorbies() {
		return this.foundChorbies;
	}
	public void setFoundChorbies(final Collection<Chorbi> foundChorbies) {
		this.foundChorbies = foundChorbies;
	}

	@Valid
	@OneToOne(optional = false)
	public Chorbi getChorbi() {
		return this.chorbi;
	}
	public void setChorbi(final Chorbi chorbi) {
		this.chorbi = chorbi;
	}

}
