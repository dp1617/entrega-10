
package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/misc")
public class MiscController extends AbstractController {

	// Methods

	@RequestMapping("/terms")
	public ModelAndView terms() {
		ModelAndView result;

		result = new ModelAndView("misc/terms");

		return result;
	}

	@RequestMapping("/aboutUs")
	public ModelAndView aboutUs() {
		ModelAndView result;

		result = new ModelAndView("misc/aboutUs");

		return result;
	}
}
