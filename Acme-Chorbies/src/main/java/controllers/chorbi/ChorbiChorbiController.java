/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.chorbi;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ChorbiService;
import services.LikesService;
import services.SearchTemplateService;
import controllers.AbstractController;
import domain.Actor;
import domain.Chorbi;
import domain.Likes;
import domain.SearchTemplate;

@Controller
@RequestMapping("/chorbi/chorbi")
public class ChorbiChorbiController extends AbstractController {

	// Services

	@Autowired
	private SearchTemplateService	searchTemplateService;
	@Autowired
	private LikesService			likesService;
	@Autowired
	private ActorService			actorService;
	@Autowired
	private ChorbiService			chorbiService;


	// Constructors -----------------------------------------------------------

	public ChorbiChorbiController() {
		super();
	}

	// Index ------------------------------------------------------------------		

	// Show for searchTemplate
	@RequestMapping(value = "/foundChorbies", method = RequestMethod.GET)
	public ModelAndView show() {
		ModelAndView result;
		Chorbi chorbi;
		SearchTemplate searchTemplate;
		Collection<Chorbi> foundChorbies = new ArrayList<>();
		Collection<Chorbi> res = new ArrayList<>();

		try {

			chorbi = this.searchTemplateService.findChorbiByPrincipal();

			this.searchTemplateService.checkCreditCard(chorbi);

			searchTemplate = chorbi.getSearchTemplate();
			foundChorbies = this.searchTemplateService.getSearchTemplateResults(searchTemplate);
			
			res = this.chorbiService.getChorbiesWithStars(foundChorbies);
			
			Actor actor;

			actor = this.actorService.findByPrincipal();

			if (actor instanceof Chorbi)
				res.remove(actor);

			result = this.createShowModelAndView(res);

		} catch (final Throwable oops) {

			final Collection<Chorbi> chorbies = new ArrayList<>();

			if (oops.getLocalizedMessage().contains("La tarjeta de cr�dito no es v�lida, cambiala antes de continuar"))
				result = this.createShowModelAndView(chorbies, "chorbi.creditCard.error");
			else
				result = this.createShowModelAndView(chorbies);
		}

		return result;
	}

	// Show liking chorbies
	@RequestMapping(value = "/likes", method = RequestMethod.GET)
	public ModelAndView showLikingChorbis(@RequestParam final int chorbiID) {
		ModelAndView result;

		Collection<Chorbi> foundChorbies = new ArrayList<>();
		Collection<Chorbi> res = new ArrayList<>();
		Collection<Likes> commentsLikes = new ArrayList<>();

		foundChorbies = this.likesService.findLikingChorbies(chorbiID);
		commentsLikes = this.likesService.findComments(chorbiID);

		res = this.chorbiService.getChorbiesWithStars(foundChorbies);

		result = this.createShowModelAndView(res);
		result.addObject("likes", commentsLikes);

		return result;
	}

	// Auxiliar methods --------------------------------------------

	// Create model and view for show
	protected ModelAndView createShowModelAndView(final Collection<Chorbi> chorbies) {
		ModelAndView result;

		result = this.createShowModelAndView(chorbies, null);

		return result;
	}
	protected ModelAndView createShowModelAndView(final Collection<Chorbi> chorbies, final String message) {
		ModelAndView result;
		Actor actor;

		actor = this.actorService.findByPrincipal();

		result = new ModelAndView("chorbi/chorbi/foundChorbies");
		if (actor instanceof Chorbi)
			result.addObject("toIterate", this.chorbiService.findByPrincipal().getSentLikes());
		result.addObject("chorbies", chorbies);
		result.addObject("message", message);
		result.addObject("requestURI", "chorbi/chorbi/foundChorbies.do");

		return result;
	}

}
