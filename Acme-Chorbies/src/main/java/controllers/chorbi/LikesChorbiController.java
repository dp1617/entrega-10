
package controllers.chorbi;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ChorbiService;
import services.LikesService;
import controllers.AbstractController;
import domain.Chorbi;
import domain.Likes;

@Controller
@RequestMapping("/likes/chorbi")
public class LikesChorbiController extends AbstractController {

	// Services -------------------------------------------

	@Autowired
	private LikesService	likesService;
	@Autowired
	private ChorbiService	chorbiService;


	// Crear un like
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int chorbiId) {
		ModelAndView res;
		Likes likes;
		Chorbi chorbi;

		chorbi = this.chorbiService.findOne(chorbiId);
		likes = this.likesService.create(chorbi);

		res = this.createModelAndView(likes);

		return res;
	}

	// Guardar
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Likes like, final BindingResult binding, final HttpServletRequest request) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(like);
			System.out.println(binding.getAllErrors());
		} else
			try {

				this.likesService.sendLike(like);

				if (request.getHeader("referer").contains("foundChorbies"))
					res = new ModelAndView("redirect:/chorbi/chorbi/foundChorbies.do");
				else
					res = new ModelAndView("redirect:/chorbi/list.do");

			} catch (final Throwable oops) {
				res = this.createModelAndView(like, "like.error");
			}

		return res;
	}
	// Guardar en la base de datos el nuevo like
	@RequestMapping(value = "/cancelLikes", method = RequestMethod.GET)
	public ModelAndView cancelLikes(@RequestParam final int chorbiId, final HttpServletRequest request) {
		ModelAndView res;
		Chorbi me;
		Chorbi cancelChorbi;

		Likes like = null;

		me = this.chorbiService.findByPrincipal();
		cancelChorbi = this.chorbiService.findOne(chorbiId);

		for (final Likes l : me.getSentLikes())
			if (l.getLikedChorbi().getId() == cancelChorbi.getId())
				like = l;

		this.likesService.checkLike2(like);

		this.likesService.delete(like);

		if (request.getHeader("referer").contains("foundChorbies"))
			res = new ModelAndView("redirect:/chorbi/chorbi/foundChorbies.do");
		else
			res = new ModelAndView("redirect:/chorbi/list.do");

		return res;
	}

	//Ancillary methods

	protected ModelAndView createModelAndView(final Likes like) {
		ModelAndView result;

		result = this.createModelAndView(like, null);

		return result;
	}
	protected ModelAndView createModelAndView(final Likes like, final String message) {
		ModelAndView result;

		result = new ModelAndView("chorbi/like/create");

		result.addObject("like", like);
		result.addObject("message", message);

		return result;
	}
}
