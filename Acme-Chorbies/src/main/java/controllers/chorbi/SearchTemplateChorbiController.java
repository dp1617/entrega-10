/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.chorbi;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.SearchTemplateService;
import controllers.AbstractController;
import domain.Chorbi;
import domain.SearchTemplate;

@Controller
@RequestMapping("/searchTemplate/chorbi")
public class SearchTemplateChorbiController extends AbstractController {

	// Services

	@Autowired
	private SearchTemplateService	searchTemplateService;


	// Constructors -----------------------------------------------------------

	public SearchTemplateChorbiController() {
		super();
	}

	// Index ------------------------------------------------------------------		

	// Show for searchTemplate
	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show() {
		ModelAndView result;
		Chorbi chorbi;
		SearchTemplate searchTemplate;

		chorbi = this.searchTemplateService.findChorbiByPrincipal();
		searchTemplate = chorbi.getSearchTemplate();

		result = new ModelAndView("searchTemplate/chorbi/show");
		result.addObject("searchTemplate", searchTemplate);
		result.addObject("requestURI", "searchTemplate/chorbi/show.do");

		return result;
	}

	// Edit for searchTemplate
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int searchTemplateId) {
		ModelAndView result;
		SearchTemplate searchTemplate;

		searchTemplate = this.searchTemplateService.findOne(searchTemplateId);

		result = this.createEditModelAndView(searchTemplate);

		return result;
	}
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final SearchTemplate searchTemplate, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createBindingModelAndView(searchTemplate, "typeMismatch.searchTemplate.estimatedAge");
		else
			try {
				this.searchTemplateService.checkActualChorbiAuthor(searchTemplate);
				this.searchTemplateService.save(searchTemplate);
				result = new ModelAndView("redirect:/searchTemplate/chorbi/show.do");
			} catch (final Throwable oops) {
				if (oops.getLocalizedMessage().contains("No eres el autor"))
					result = this.createEditModelAndView(searchTemplate, "searchTemplate.author.error");
				else
					result = this.createEditModelAndView(searchTemplate, "searchTemplate.commit.error");
			}
		return result;
	}

	// Auxiliary methods ---------------------------------------------------------

	// Create model and view for edit
	protected ModelAndView createEditModelAndView(final SearchTemplate searchTemplate) {
		ModelAndView result;

		result = this.createEditModelAndView(searchTemplate, null);

		return result;
	}
	protected ModelAndView createEditModelAndView(final SearchTemplate searchTemplate, final String message) {
		ModelAndView result;
		Chorbi chorbi;

		chorbi = this.searchTemplateService.findChorbiByPrincipal();
		result = new ModelAndView("searchTemplate/chorbi/edit");

		result.addObject("chorbi", chorbi);
		result.addObject("searchTemplate", searchTemplate);
		result.addObject("message", message);

		return result;
	}

	// Create model and view for binding
	protected ModelAndView createBindingModelAndView(final SearchTemplate searchTemplate) {
		ModelAndView result;

		result = this.createBindingModelAndView(searchTemplate, null);

		return result;
	}
	protected ModelAndView createBindingModelAndView(final SearchTemplate searchTemplate, final String message) {
		ModelAndView result;
		Chorbi chorbi;

		chorbi = this.searchTemplateService.findChorbiByPrincipal();
		result = new ModelAndView("searchTemplate/chorbi/edit");

		result.addObject("chorbi", chorbi);
		result.addObject("searchTemplate", searchTemplate);

		return result;
	}
}
