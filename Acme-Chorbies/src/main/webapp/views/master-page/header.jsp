<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<div>
	<img src="images/logo.png" alt="Acme Chorbies Co., Inc." />
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="hasRole('ADMIN')">
		<li><a class="fNiv"><spring:message
						code="master.page.systemConfiguration" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="systemConfiguration/edit.do"><spring:message
								code="master.page.systemConfiguration.edit" /></a></li>
				</ul></li>
		</security:authorize>


		<security:authorize access="hasRole('CHORBI')">

			<li><a class="fNiv"><spring:message
						code="master.page.searchTemplate" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="searchTemplate/chorbi/show.do"><spring:message
								code="master.page.chorbi.searchTemplate.show" /></a></li>
					<li><a href="chorbi/chorbi/foundChorbies.do"><spring:message
								code="master.page.chorbi.chorbi.foundChorbies" /></a></li>
				</ul></li>

			<li><a class="fNiv"><spring:message
						code="master.page.chirps" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="chirp/sent.do"><spring:message
								code="master.page.chirps.sent" /></a></li>
					<li><a href="chirp/received.do"><spring:message
								code="master.page.chirps.received" /></a></li>
					<li><a href="chirp/create.do"><spring:message
								code="master.page.chirps.create" /></a></li>
				</ul></li>

		</security:authorize>

		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message
						code="master.page.login" /></a></li>
			<li><a href="chorbi/create.do"><spring:message
					code="master.page.chorbi.create" /></a></li>
		</security:authorize>

		<security:authorize access="isAuthenticated()">
		
			<li><a class="fNiv"><spring:message
						code="master.page.chorbies" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="chorbi/list.do"><spring:message
								code="master.page.chorbies.list" /></a></li>
				</ul></li>
			<li><a class="fNiv"> <spring:message
						code="master.page.profile" /> (<security:authentication
						property="principal.username" />)
			</a>
				<ul>
					<li class="arrow"></li>
					<security:authorize access="hasRole('CHORBI')">
						<li><a href="chorbi/edit.do"><spring:message
									code="master.page.chorbi.edit" /></a></li>
						<li><a href="chorbi/addCreditCard.do"><spring:message
									code="master.page.chorbi.creditcard" /></a></li>
					
					</security:authorize>
					<security:authorize access="hasRole('ADMIN')">
						<li><a href="administrator/dashboard.do"><spring:message
									code="master.page.administrator.dashboard" /></a></li>
					</security:authorize>
					<li><a href="j_spring_security_logout"><spring:message
								code="master.page.logout" /> </a></li>
				</ul></li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

