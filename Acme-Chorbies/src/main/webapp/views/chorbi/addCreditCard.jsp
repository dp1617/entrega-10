<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="chorbi/addCreditCard.do" modelAttribute="chorbi">

	<!-- atributos -->

	<!-- Datos para la credit card -->

	<br />

	<acme:textbox code="chorbi.holderName" path="holderName" />
	<acme:textbox code="chorbi.brandName" path="brandName" placeholder="VISA, MASTERCARD, AMEX, DINNERS or DISCOVER"/>
	<acme:textbox code="chorbi.number" path="number" />
	<acme:textbox code="chorbi.expirationMonth" path="expirationMonth" />
	<acme:textbox code="chorbi.expirationYear" path="expirationYear" />
	<acme:textbox code="chorbi.cvvCode" path="cvvCode" />

	<br />
	
	<!-- botones -->
	<acme:submit name="save" code="chorbi.save" />

	<acme:cancel url="" code="chorbi.cancel" />

</form:form>
