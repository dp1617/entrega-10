<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Creaci�n de chorbi (como usuario no autentificado) -->

<form:form action="likes/chorbi/create.do" modelAttribute="like">

	<form:hidden path="id"/>
	<form:hidden path="version"/>
	<form:hidden path="likedChorbi"/>
	<form:hidden path="likingChorbi"/>
	<form:hidden path="likedMoment"/>

	<acme:textarea code="like.comment" path="comment" />


	<!-- Acciones -->
	
	<acme:submit name="save" code="like.save"/>
	
	<acme:cancel url="" code="like.cancel"/>

</form:form>

<br>

<!-- Errores -->
